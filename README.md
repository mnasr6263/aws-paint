# Setting up AWS Machine(ec2) with paint 
In this project we will make a bash script to provision a machine in aws to run js paint. 

## Subjects Covered 
This project will help cover:
1. Git 
2. Bitbucket
3. Markdown 
4. AWS 
   1. ec2 creation 
   2. Security groups 
5. Redhat 
   1. Installation process 
   2. Scripts
6. Bash 
   1. ssh
   2. scp
   3. heredoc 
   4. Outputs (stdin, stdout, stderr)
   5. Piping and Grep 
---
 
## Reverse Proxy
Reverse proxy has two main use cases:
- Send traffic from one port, such as port 8080, to port 80.
- Be a proxy server and redirect traffic to different servers. This is known as load balancing. Useful to manage high traffic and maintain high availability.    
---


## Installing JSpain on to AWS webserver

1. Install git 
```

$ sudo yum -y install git 
```
2. Cloned git 
```
$ git clone https://github.com/1j01/jspaint.git
$ cd jspaint 


```

3. Install nodejs 
```
$ VERSION=v14.16.1
$ DISTRO=linux-x64
$ sudo mkdir -p /usr/local/lib/nodejs
$ wget https://nodejs.org/dist/v14.16.1/node-v14.16.1-linux-x64.tar.xz
$ sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 
$ npm i ~/.bash_profile


$ echo 'VERSION=v14.16.1
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH' >> /.bash_profile 
source ~/.bash_profile 
node -v 
```

4. Reverse proxy
```
$ sudo yum -y install httpd 
$ 
$ cd /etc/httpd/conf.d 
$ touch jspaint.conf 
$ echo '<VirtualHost *:80>
    ProxyPreserveHost On

    ProxyPass / http://127.0.0.1:8080
</VirtualHost>' >> jspaint.conf 
$ sudo systemctl restart httpd
$ cd jspaint 
$ npm i (install dependancies )
$ npm run dev 
```

---
## Explanation of Commands
- < is opening a file, << is opening an end of input marker.
- chkconfig command is used to list all available services and view or update their run level setting. 
- chkconfig: 2345 99 99 Make this file available at boot levels 2-5, when to start (99 i.e. last thing to start) and when to stop (99 i.e. first process to stop).
- case - the case constuct allows us to test strings against patterns. In our case it checks $1 and matches to conditions listed (start/stop).
- ;; stops flow beyond that point. 
- nohup means run it no matter what.
- egrep - extended grep, check for node or npm { } stops shell from running it as a pipe, use it as an or for egrep ????
- awk '{print $2}' extracts output looking for only the second column which is the processID ?
- & runs command in the background. 


What is a service?
A program that runs in the background 



