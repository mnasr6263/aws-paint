#!/bin/bash 

if (( $# > 0)) # Evaluates a condition (returns true/false/binary)
then
    hostname=$1 # $ is input argument in command line 
else 
    echo "WTF: you must supply a hostname or IP address" 1>&2 # Move and merge standard output to standard error. 
    exit 1 # Identify it as a failure 
fi

# -i identity key, -o options, we are using stricthostkeychecking to stop it from asking for fingerprint, 
ssh -o StrictHostKeyChecking=no -i ~/.ssh/MohamedNasrKey.pem ec2-user@$hostname  '

 
sudo yum -y install httpd 

if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
  sudo systemctl start httpd
else 
    exit 1 
fi

sudo yum -y install git 

git clone https://github.com/1j01/jspaint.git
cd jspaint

VERSION=v14.16.1
DISTRO=linux-x64
sudo mkdir -p /usr/local/lib/nodejs
wget https://nodejs.org/dist/$VERSION/node-$VERSION-$DISTRO.tar.xz
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 

echo 'VERSION=v14.16.1
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/ bin:$PATH' >> /.bash_profile 
source ~/.bash_profile 
 
npm install 

'
# quotes allow you to run several commands on the virtual machine
# The : is used for ease of separating paths

#Get the code 
#Relevant code compiler (npm)
#Install dependancies 
#Start the server (init script)
#Setup nodejs
#Setup reverse proxy 

## Installing JSpain on to AWS webserver

# 1. Install git 
```


```
# 2. Cloned git 
```
 


```
# 3. Install nodejs 
```



$ 
```
```
# 4. Reverse proxy



$ cd /etc/httpd/conf.d 
$ sudo sh -c "echo '<VirtualHost *:80>
    ProxyPreserveHost On

    ProxyPass / http://127.0.0.1:8080
</VirtualHost>' > jspaint.conf" 
$ sudo systemctl restart httpd
$ cd jspaint 
$ npm i (install dependancies )


$ npm run dev 
```
# run your webserver and check 

# sh -c, shell command - launching a shell and telling the shell to run the command in quotes. Used when trying to run commands with sudo.
